package testScripts.LACO.Finance.Payments;

import java.io.IOException;
import java.text.ParseException;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import commonPageObjects.NonComp.Claims.Claimants;
import commonPageObjects.NonComp.Claims.Claims;
import commonPageObjects.NonComp.Claims.SubClaims;
import commonPageObjects.NonComp.Finance.Payments;
import commonPageObjects.NonComp.Finance.Reserves;
import commonPageObjects.NonComp.IRF.IRF_ENS;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CreatePaymentTestScripts {

	private static final boolean execution = true;
	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	Dashboard dashboardPageObjects;
	IRF_ENS irfPageObjects;
	WebDriver driver;
	BaseClass bc;
	PolicySetUp policySetupPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	Claims claimScreenPageObjects;
	Claimants claimantScreenPageObjects;
	SubClaims subClaimPageObjects;
	SearchClaim searchClaimPageObject;
	Reserves reservePageObject;
	Payments paymentPageObject;
	String reserveCategory = null;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		irfPageObjects = PageFactory.initElements(driver, IRF_ENS.class);
		policySetupPageObject = PageFactory.initElements(driver, PolicySetUp.class);
		policyItem = PageFactory.initElements(driver, PolicyItem.class);
		policyItemCoverges = PageFactory.initElements(driver, PolicyItemCoverages.class);
		claimantScreenPageObjects = PageFactory.initElements(driver, Claimants.class);
		subClaimPageObjects = PageFactory.initElements(driver, SubClaims.class);
		claimScreenPageObjects = PageFactory.initElements(driver, Claims.class);
		dashboardPageObjects = loginPageObjects.Login(8);
		dashboardPageObjects = PageFactory.initElements(driver, Dashboard.class);
		reservePageObject = PageFactory.initElements(driver, Reserves.class);
		paymentPageObject = PageFactory.initElements(driver, Payments.class);
		dashboardPageObjects.selectAgree();
		searchClaimPageObject=dashboardPageObjects.navigateToClaimSearch();
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@Test(priority = 1, enabled = execution)
	public void enterReserveForWC() throws InterruptedException {
		logger = report.startTest("Verify user is able to enter the reserves successfully");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectWC_Claim();
		Wait.waitFor(7);
		logger.log(LogStatus.INFO, "Select the claim");
		dashboardPageObjects.clickReserveForWC();
		logger.log(LogStatus.INFO, "Click the reserve tab");
		Wait.waitFor(2);
		reservePageObject.getWindowForReserveWCScreen();
		Wait.waitFor(2);
		reservePageObject.clickWorksheetforWCReserves();
		Wait.waitFor(2);
		reservePageObject.switchToWorkSheetScreenForWC();
		Wait.waitFor(2);
		System.out.println(driver.getTitle());
		logger.log(LogStatus.INFO, "Clicks on worksheet button");
		Wait.waitFor(3);
		reservePageObject.getWindowForReserveWCWorksheet();
		String enteredReserveAmount = reservePageObject.enterNewReservesWC();
		logger.log(LogStatus.INFO, "Enter the reerve amount");
		reservePageObject.enterReasonWCCode();
		logger.log(LogStatus.INFO, "Select the reason code from drop down");

		Wait.waitFor(2);
		reservePageObject.clickPostChangesButton();
		logger.log(LogStatus.INFO, "Click on post changes button");
		Wait.waitFor(5);
		reservePageObject.switchToReserveNotesWindow();
		reservePageObject.closeWindow();
		Wait.waitFor(3);
		reservePageObject.switchToReserveScreenAgain();
		Wait.waitFor(2);
		reservePageObject.refresh();
		Wait.waitFor(2);
		String actualReserveAmount = reservePageObject.fetchWCReserveAmount();

		System.out.println("actual Reserve Amount: " + actualReserveAmount);
		System.out.println("entered Reserve Amount: " + "$" + enteredReserveAmount + ".00");
		Wait.waitFor(3);
		Assert.assertEquals(actualReserveAmount, "$" + enteredReserveAmount + ".00");
		logger.log(LogStatus.PASS, "Verified that reserve has been added successfully");
	}

	@Test(priority = 2, enabled = execution, dependsOnMethods = "enterReserveForWC")
	public void enterPaymentForWC() throws InterruptedException, ParseException {
		reserveCategory = reservePageObject.fetchWCReserveCategory();
		System.out.println(reserveCategory);
		String amtBeforePayment = reservePageObject.fetchPaymentAmtForWC();
		System.out.println("Amount Before Payment: " + amtBeforePayment);
		logger = report.startTest("Verify user is able to make one time  payment successfully");
		dashboardPageObjects.clickPaymentForWC();
		logger.log(LogStatus.INFO, "Navigate To payment Screen");
		paymentPageObject.selectPaymentTypeAsOneTime();
		Wait.waitFor(3);
		logger.log(LogStatus.INFO, "Selected Payment Type as One Time");
		paymentPageObject.selectPayeeType();
		Wait.waitFor(10);
		logger.log(LogStatus.INFO, "Selected Payee Type as Claimant");
		paymentPageObject.selectModeType();
		Wait.waitFor(10);
		/* Wait.waitTillPresent(driver, paymentPageObject.); */
		paymentPageObject.selectPaymentMode();
		logger.log(LogStatus.INFO, "Selected Mode Type");
		paymentPageObject.enterPaymentFrom();
		logger.log(LogStatus.INFO, "Enter paymemt from");
		paymentPageObject.enterPaymentThru();
		logger.log(LogStatus.INFO, "Enter paymemt thru");
		paymentPageObject.enterCheckDate();
		logger.log(LogStatus.INFO, "Enter check Date");
		paymentPageObject.enterCheckNumber();
		logger.log(LogStatus.INFO, "Enter check Number");
		paymentPageObject.selectpayCatForWC(reserveCategory);
		Wait.waitFor(10);
		logger.log(LogStatus.INFO, "Selected Pay Category");
		paymentPageObject.selectpaySubCatForWC();
		Wait.waitFor(5);
		logger.log(LogStatus.INFO, "Selected Pay Sub Category");
		String expected = paymentPageObject.enterPayAmtForWC();
		Float amtBeforePay = Float.parseFloat(amtBeforePayment.replaceAll("[^0-9.]", ""));
		System.out.println(amtBeforePay);
		Float expectedValue = Float.parseFloat(expected);
		System.out.println(expectedValue);
		Float finalExpected = amtBeforePay + expectedValue;
		System.out.println(finalExpected);
		logger.log(LogStatus.INFO, "Enter Pay Amt.");
		paymentPageObject.clickSaveButton();
		logger.log(LogStatus.INFO, "Click Save Button");
		bc.BrowserScrollup();
		Wait.waitFor(5);
		if (paymentPageObject.fetchMessage().startsWith("Duplicate")) {
			char result = paymentPageObject.clickDraftOrContinue();
			if (result == 'd') {
				Wait.modifyWait(driver, paymentPageObject.messageLabel);
				Wait.waitFor(6);
				Assert.assertEquals("$"+paymentPageObject.fetchDraftAmount(), "$" + expected + ".00");
			} else if (result == 'c') {
				dashboardPageObjects.clickReserveForWC();
				Wait.waitFor(10);
				String finalExpectdPaidAmount = paymentPageObject.makeExpectedPaidAmount(finalExpected);
				Assert.assertEquals(reservePageObject.fetchPaymentAmtForWC().replaceAll(",", ""),
						finalExpectdPaidAmount);
				logger.log(LogStatus.PASS, "Verified that Pay Amt has been created successfuly");
			}
		} else {
			dashboardPageObjects.clickReserveForWC();
			Wait.waitFor(10);
			String finalExpectdPaidAmount = paymentPageObject.makeExpectedPaidAmount(finalExpected);
			Assert.assertEquals(reservePageObject.fetchPaymentAmtForWC().replaceAll(",", ""), finalExpectdPaidAmount);
			logger.log(LogStatus.PASS, "Verified that Pay Amt has been created successfuly");
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		// driver.quit();
	}

}


