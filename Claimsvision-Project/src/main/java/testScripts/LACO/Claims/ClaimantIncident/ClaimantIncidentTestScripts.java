package testScripts.LACO.Claims.ClaimantIncident;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import commonPageObjects.WorkersComp.Claimant.Claimant;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class ClaimantIncidentTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	SearchClaim searchClaimPageObject;
	Dashboard dashboardPageObjects;
	Claimant claimantObject;
	WebDriver driver;
	BaseClass bc;
	
	public final boolean execution=true;
	
	{

		report=ReportManager.getReporter();
	}
	
/*	@BeforeClass
	public void beforeClass() throws Exception, Exception
	{		
		driver=Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		claimantObject = PageFactory.initElements(driver, Claimant.class);
		searchClaimPageObject = PageFactory.initElements(driver, SearchClaim.class);
		dashboardPageObjects = loginPageObjects.Login(8);
		dashboardPageObjects.selectAgree();
		
	}*/
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		driver=Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		claimantObject = PageFactory.initElements(driver, Claimant.class);
		searchClaimPageObject = PageFactory.initElements(driver, SearchClaim.class);
		dashboardPageObjects = loginPageObjects.Login(8);
		dashboardPageObjects.selectAgree();
		
	
	}
	
	@Test(priority=1, enabled = execution)
	public void testUpdateFunctionality() throws Exception{
		logger = report.startTest("Verify user is able to update Claim details successfully");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		claimantObject.enterClaimantFirstName();
		logger.log(LogStatus.INFO, "User entered the claimant first name");
		claimantObject.enterClaimantMiddleName();
		logger.log(LogStatus.INFO, "User entered the claimant middle name");
		claimantObject.enterClaimantLastName();		
		logger.log(LogStatus.INFO, "User entered the claimant last name");
		claimantObject.enterClaimantSuffix();
		logger.log(LogStatus.INFO, "User entered the claimant suffix");
		claimantObject.enterClaimantEmailAddress();
		logger.log(LogStatus.INFO, "User entered the claimant email address");
		claimantObject.enterClaimantPreferredName();
		logger.log(LogStatus.INFO, "User entered the claimant preferred name");
		claimantObject.enterUniqueISOClaim();
		logger.log(LogStatus.INFO, "User entered the Unique ISO Claim");
		claimantObject.enterClaimantID();
		logger.log(LogStatus.INFO, "User entered the claimant ID");
		claimantObject.selectClaimantIdType();
		logger.log(LogStatus.INFO, "User selected the claimant ID Type");
		claimantObject.enterEmployeeID();
		logger.log(LogStatus.INFO, "User entered the employee id");
		claimantObject.selectSSNRelease();
		logger.log(LogStatus.INFO, "User selected the SSN Release checkbox");
		claimantObject.selectMedicalRecordRelease();
		logger.log(LogStatus.INFO, "User selected the Medical Record Release checkbox");
		claimantObject.enterClaimantAddress1();
		logger.log(LogStatus.INFO, "User entered the claimant address 1");
		claimantObject.enterClaimantAddress2();
		logger.log(LogStatus.INFO, "User entered the claimant address 2");
		claimantObject.enterHomePhone();
		logger.log(LogStatus.INFO, "User entered home phone number");
		claimantObject.enterCellPhone();
		logger.log(LogStatus.INFO, "User entered cell phone number");
		claimantObject.selectCompensable();
		logger.log(LogStatus.INFO, "User selected Compensable");
		claimantObject.selectAdjuster();
		logger.log(LogStatus.INFO, "User selected Adjuster");
		claimantObject.selectSupervisor();
		logger.log(LogStatus.INFO, "User selected Supervisor");
		claimantObject.selectConversionStatus();		
		logger.log(LogStatus.INFO, "User selected Claim Conversion Status");
		claimantObject.selectSex();
		logger.log(LogStatus.INFO, "User selected sex");
		claimantObject.selectMaritalStatus();
		logger.log(LogStatus.INFO, "User selected Marital Status");
		String dateOfIncident = claimantObject.enterIncidentDate();
		logger.log(LogStatus.INFO, "User entered date of Incident");
		claimantObject.enterDateOfBirth(dateOfIncident);
		logger.log(LogStatus.INFO, "User entered date of birth");
		claimantObject.enterNumOfDepts();
		logger.log(LogStatus.INFO, "User entered number of dependents");
		claimantObject.selectTimeOfIncident();
		logger.log(LogStatus.INFO, "User selected time of incident");
		claimantObject.enterDateOfKnowledge();
		logger.log(LogStatus.INFO, "User entered date of Knowledge");
		claimantObject.enterIncidentDesc();
		logger.log(LogStatus.INFO, "User entered incident description");
		claimantObject.selectCauseOfIncidentMajor();
		logger.log(LogStatus.INFO, "User selected Cause of incident(major)");
		claimantObject.selectCauseOfIncidentMinor();
		logger.log(LogStatus.INFO, "User selected Cause of incident(minor)");
		claimantObject.selectNatureOfIncidentMajor();
		logger.log(LogStatus.INFO, "User selected Nature of incident(major)");
		claimantObject.selectNatureOfIncidentMinor();
		logger.log(LogStatus.INFO, "User selected Nature of incident(minor)");
		claimantObject.selectPartOfBodyMajor();
		logger.log(LogStatus.INFO, "User selected Part of Body(major)");
		claimantObject.selectPartOfBodyMinor();
		logger.log(LogStatus.INFO, "User selected Part of Body(minor)");
		claimantObject.enterBodyPerCent();
		logger.log(LogStatus.INFO, "User entered body percent");
		claimantObject.enterClaimSummary();
		logger.log(LogStatus.INFO, "User entered claim summary");
		claimantObject.selectFatalityReason();
		logger.log(LogStatus.INFO, "User selected fatality reason");
		claimantObject.enterDeathDate(dateOfIncident);
		logger.log(LogStatus.INFO, "User entered death date");
		claimantObject.enterInitialReportedDate(dateOfIncident);
		logger.log(LogStatus.INFO, "User entered initial reported date");
		claimantObject.enterLastReportedDate(dateOfIncident);
		logger.log(LogStatus.INFO, "User entered last reported date");
		claimantObject.enterHICNNumber();
		logger.log(LogStatus.INFO, "User entered HICN Number");
		claimantObject.enterFirstExposureDate();
		logger.log(LogStatus.INFO, "User entered first exposure date");
		claimantObject.enterLastExposureDate();
		logger.log(LogStatus.INFO, "User entered last exposure date");
		claimantObject.selectAcquiredClaim();
		logger.log(LogStatus.INFO, "User selected acquired claim");
		claimantObject.selectHoldExtract();
		logger.log(LogStatus.INFO, "User selected hold extract");
		claimantObject.selectContinuousTrauma();
		logger.log(LogStatus.INFO, "User selected continuous trauma");
		claimantObject.selectISOExclude();
		logger.log(LogStatus.INFO, "User selected ISO Exclude");
		claimantObject.selectFROILateReason();
		logger.log(LogStatus.INFO, "User selected FROI late reason");
		claimantObject.selectClaimSuspended();
		logger.log(LogStatus.INFO, "User selected claim suspended");
		Wait.waitFor(3);
		claimantObject.selectSuspensionLevel();
		logger.log(LogStatus.INFO, "User selected suspension level");
		claimantObject.selectSuspensionType();
		logger.log(LogStatus.INFO, "User selected suspension type");
		claimantObject.enterSuspensionEffDate();
		logger.log(LogStatus.INFO, "User entered suspension effective date");
		claimantObject.enterSuspensionNarrative();
		logger.log(LogStatus.INFO, "User entered suspension narrative");
		claimantObject.selectClaimRescinded();
		logger.log(LogStatus.INFO, "User selected claim rescinded");
		claimantObject.enterRescissionDate();
		logger.log(LogStatus.INFO, "User enter rescission date");
		Wait.waitFor(3);
		claimantObject.clickOnSaveButton();
		System.out.println("Values entered successfully");
		logger.log(LogStatus.INFO, "Claimant details updated successfully");
		Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.validationMessage.getText(), "Saved successfully.");
		logger.log(LogStatus.PASS, "Test Pass for Claimant details updates");
	}
	
	@Test(priority=2, enabled = execution)
	public void testClaimStatustoCloseThroughSaveIcon() throws Exception{
		logger = report.startTest("Verify user is able to close claim through Save Icon");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		claimantObject.selectStatusOfClaimAsClosed();
		logger.log(LogStatus.INFO, "User selected status of Claim");
		claimantObject.selectClosedReason();
		logger.log(LogStatus.INFO, "User selected Closed Reason");
		Wait.waitFor(3);
		claimantObject.clickOnSaveIcon();
		logger.log(LogStatus.INFO, "User clicked on Save Icon");
		Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.getClaimStatus(), "Closed");
		
	}
	
	@Test(priority=3, enabled = execution)
	public void testClaimStatustoCloseThroughSaveButton() throws Exception{
		logger = report.startTest("Verify user is able to close claim through Save Icon");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		claimantObject.selectStatusOfClaimAsClosed();
		logger.log(LogStatus.INFO, "User selected status of Claim");
		claimantObject.selectClosedReason();
		logger.log(LogStatus.INFO, "User selected Closed Reason");
		Wait.waitFor(3);
		claimantObject.clickOnSaveButton();
		logger.log(LogStatus.INFO, "User clicked on Save Icon");
		Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.getClaimStatus(), "Closed");
		
	}
	
	@Test(priority=4, enabled = execution)
	public void testClaimStatustoCloseThroughCloseIcon() throws Exception{
		logger = report.startTest("Verify user is able to close claim through Save Icon");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		
		claimantObject.clickOnCloseClaimIcon();
		logger.log(LogStatus.INFO, "User clicked on Close Claim icon");
		claimantObject.selectClosedReason();
		logger.log(LogStatus.INFO, "User selected Closed Reason");
		Wait.waitFor(3);
		//claimantObject.clickOnSaveIcon();
		//logger.log(LogStatus.INFO, "User clicked on Save Icon");
		Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.validationMessage.getText(), "The claim was closed successfully.");
		
	}
	
	@Test(priority=5, enabled = execution)
	public void testClaimStatustoReOpenThroughOpenIcon() throws Exception{
		logger = report.startTest("Verify user is able to close claim through Save Icon");
		searchClaimPageObject.selectClaimStatus(1);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		claimantObject.clickOnOpenClaimIcon();
		Wait.waitFor(5);
		//claimantObject.clickOnSaveIcon();
		//logger.log(LogStatus.INFO, "User clicked on Save Icon");
		//Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.validationMessage.getText(), "The claim was opened successfully.");
		
	}
	/*
	@Test(priority=3, enabled = execution)
	public void testClaimStatustoCloseThroughSaveButton() throws Exception{
		logger = report.startTest("Verify user is able to close claim through Save Icon");
		searchClaimPageObject.selectClaimStatus();
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		claimantObject.selectStatusOfClaim();
		logger.log(LogStatus.INFO, "User selected status of Claim");
		claimantObject.selectClosedReason();
		logger.log(LogStatus.INFO, "User selected Closed Reason");
		Wait.waitFor(3);
		claimantObject.clickOnSaveButton();
		logger.log(LogStatus.INFO, "User clicked on Save Icon");
		Wait.waitTillPresent(driver, claimantObject.validationMessage);
		Assert.assertEquals(claimantObject.getClaimStatus(), "Closed");
		
	}*/
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		//driver.quit();
	}
	
	

}
