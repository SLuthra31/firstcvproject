package testScripts.LACO.Claims.Notes;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.CommonModules.Notes;
import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import commonPageObjects.WorkersComp.Claimant.Claimant;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class NotesWCTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	SearchClaim searchClaimPageObject;
	Dashboard dashboardPageObjects;
	Notes notesWC;
	WebDriver driver;
	BaseClass bc;
	
	public final boolean execution=true;
	
	{

		report=ReportManager.getReporter();
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		driver=Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		notesWC = PageFactory.initElements(driver, Notes.class);
		searchClaimPageObject = PageFactory.initElements(driver, SearchClaim.class);
		dashboardPageObjects = loginPageObjects.Login(8);
		dashboardPageObjects.selectAgree();
		
	
	}
	
	@Test(priority=1, enabled = execution)
	public void testCreationOfNotes() throws Exception{
		logger = report.startTest("Verify user is able to create notes successfully");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		notesWC.clickOnNotesWC();
		logger.log(LogStatus.INFO, "User navigated to Notes screen");
		Wait.waitFor(5);
		notesWC.selectCategory();
		logger.log(LogStatus.INFO, "User selected category");
		Wait.waitFor(5);
		notesWC.selectSubCategory();
		logger.log(LogStatus.INFO, "User selected sub-category");
		notesWC.enterSubject();
		logger.log(LogStatus.INFO, "User entered Subject");
		notesWC.enterNotesText();
		logger.log(LogStatus.INFO, "User entered Notes text");
		notesWC.clickOnSave();
		logger.log(LogStatus.INFO, "User clicked on Save button");
		Wait.waitTillPresent(driver, notesWC.validationMessage);
		Assert.assertEquals(notesWC.validationMessage.getText(), "Saved successfully.");
		
		
	}
	
	@Test(priority=2, enabled = execution)
	public void testCreationOfNotesAsDraft() throws Exception{
		logger = report.startTest("Verify user is able to create notes successfully");
		searchClaimPageObject.selectClaimStatus(2);
		logger.log(LogStatus.INFO, "Navigating to Claim Search Screen");
		searchClaimPageObject.selectSearch();
		logger.log(LogStatus.INFO, "Clicked on search button");
		searchClaimPageObject.selectClaim();
		logger.log(LogStatus.INFO, "Select the claim");
		Wait.waitFor(3);
		notesWC.clickOnNotesWC();
		logger.log(LogStatus.INFO, "User navigated to Notes screen");
		Wait.waitFor(5);
		notesWC.selectCategory();
		logger.log(LogStatus.INFO, "User selected category");
		Wait.waitFor(5);
		notesWC.selectSubCategory();
		logger.log(LogStatus.INFO, "User selected sub-category");
		notesWC.enterSubject();
		logger.log(LogStatus.INFO, "User entered Subject");
		notesWC.enterNotesText();
		logger.log(LogStatus.INFO, "User entered Notes text");
		notesWC.clickOnSaveAsDraft();
		logger.log(LogStatus.INFO, "User clicked on Save as Draft button");
		Wait.waitTillPresent(driver, notesWC.validationMessage);
		Assert.assertEquals(notesWC.validationMessage.getText(), "Save as Draft successfully.");
		
		
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		//driver.quit();
	}
	
	
}
