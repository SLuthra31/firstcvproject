package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import commonPageObjects.NonComp.Claims.Claimants;
import commonPageObjects.NonComp.Claims.Claims;
import commonPageObjects.NonComp.Claims.SubClaims;
import commonPageObjects.NonComp.Finance.Reserves;
import commonPageObjects.NonComp.IRF.IRF_ENS;
import commonPageObjects.NonComp.Policy.AggregateLimitScreen;
import commonPageObjects.NonComp.Policy.PolicyAggregateLimit;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;



public class TestIncurredAmountOnAggregateScreen {
	

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyAggregateLimit policyAggregateLimitPageObject;
	Claims claimScreenPageObjects;
	AggregateLimitScreen aggregate;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	SearchClaim searchClaimPageObjects;
	Claimants claimantScreenPageObjects;
	SubClaims subClaimPageObjects;
	Reserves reservePageObject;
	IRF_ENS irfPageObjects;
	WebDriver driver;
	BaseClass bc;
	public static String effectiveDate;
	public static String expirationDate;
	String limit;
	String effDate;
	String expDate;
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		claimantScreenPageObjects=PageFactory.initElements(driver, Claimants.class);
		policyAggregateLimitPageObject = PageFactory.initElements(driver, PolicyAggregateLimit.class);
		policyItem = PageFactory.initElements(driver, PolicyItem.class);
		policyItemCoverges = PageFactory.initElements(driver, PolicyItemCoverages.class);
		aggregate = PageFactory.initElements(driver, AggregateLimitScreen.class);
		irfPageObjects = PageFactory.initElements(driver, IRF_ENS.class);
		claimScreenPageObjects = PageFactory.initElements(driver, Claims.class);
		subClaimPageObjects = PageFactory.initElements(driver, SubClaims.class);
		reservePageObject = PageFactory.initElements(driver, Reserves.class);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}

	

	@Test(priority = 1, enabled = execution)
	public void testCase01_validateIncurredAmountonAggegateScreen() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest("Test- Incurred Amount is displayed correctly on Aggregate Limit Screen");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "User Navigated to Policy number assignment screen");
			policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "Configuring Policy Number order");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button in policy number assignment screen");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
			Wait.waitFor(5);
			policySetupPageObject.enterValues();
			ArrayList<String> list = new ArrayList<String>();
			list.add(policySetupPageObject.fetchEffectiveDate());

			list.add(policySetupPageObject.fetchExpirationDate());

			list.add(policySetupPageObject.fetchCarrier());

			list.add(policySetupPageObject.fetchClient());

			list.add(policySetupPageObject.fetchPolicyType());

			list.add(policySetupPageObject.fetchShortPolicyNumber());
			effectiveDate=list.get(0);
			expirationDate=list.get(1);
			String carrierCode = policySetupPageObject.getCarrierCode(list.get(2));
			String policyCode = policySetupPageObject.getPolicyCode(list.get(4));
			String policyNum = policySetupPageObject.getExpectedPolicyNumber();
			
			policySetupPageObject.clickOnSaveButton();
			Wait.waitFor(4);
			if (policySetupPageObject.fetchMessages().startsWith("The Client has the following policies"))

			{
				logger.log(LogStatus.PASS, "Validations appears for overlapping");
				policySetupPageObject.selectContinueButton();
				logger.log(LogStatus.INFO, "Clicking on Continue button");
				Wait.waitFor(4);

			}
			policySetupPageObject.goToLastSearchResultScreen();
			Wait.waitFor(10);
			Assert.assertEquals(policySetupPageObject.fetchLatestPolicyNumber(), policyNum);
			logger.log(LogStatus.INFO, "User clicked on new policy number");
			Wait.waitFor(20);
			policySetupPageObject.getWindowForPolicySetUp();
			policyItem.clickPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Policy Item Icon");
			Wait.waitFor(3);
			policyItem.switchToPolicyItemScreen();
			Wait.waitFor(2);
			policyItem.makeItem();
			logger.log(LogStatus.INFO, "User clicks on Modify Button");
			Wait.waitFor(3);
			policyItem.getWindowHandleForItem();
			policyItemCoverges.clickOnCoverageIcon();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User clicks on Coverage Icon");
			policyItemCoverges.switchToCoverageScreen();
			Wait.waitFor(2);
			policyItemCoverges.makeCoverage();
			logger.log(LogStatus.INFO, "Add the coverage for the policy");
			policyItemCoverges.clickCloseButtonInCoveregeScreen();
			// bc.switchToNextWindow("Policy Set-up");
			policyItem.getBackToPolicyItemScreen();
			policyItem.clickCloseButtonInItemScreen();
			policySetupPageObject.getBackToPolicySetUp();
			Wait.waitFor(5);

			Browser.refresh(driver);
			Wait.waitFor(5);
			dashboardPageObject.navigateToAggregateLimitDetail();
			aggregate.selectClientValueThroughText(list.get(3));
			aggregate.selectPolicyType(policySetupPageObject.policyTypeValue);
			aggregate.selectAggregateLimitCategory();
			aggregate.selectCoverage(policyItemCoverges.coverageName);
			aggregate.selectSave();

			logger.log(LogStatus.INFO, "Assign aggregate category under the policy's aggregate in Aggregate lmit detail screen");
			Wait.waitFor(8);
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigate to policy set up screen");
			policySetupPageObject.enterPolicyNumber(policyNum);
			policySetupPageObject.searchButton2.click();
			logger.log(LogStatus.INFO, "Search the policy");
			policySetupPageObject.clickPolicy();

			policyAggregateLimitPageObject.getWindowForPolicy();
			policyAggregateLimitPageObject.clickOnAggregaeIcon();
			logger.log(LogStatus.INFO, "User clicked on Aggregate Icon");
			Wait.waitFor(3);
			
			policyAggregateLimitPageObject.switchToAggregateScreen();
			Assert.assertEquals(policyAggregateLimitPageObject.policyNumberTextBox.getAttribute("value"), policyNum);
			logger.log(LogStatus.PASS, "User is redirected to Aggregate Window");
			Wait.waitFor(2);
			policyAggregateLimitPageObject.refresh();
			policyAggregateLimitPageObject.makeCategory(this);
			logger.log(LogStatus.INFO, "Create Aggregate limit Category in the policy");
			policyItem.clickCloseButtonInItemScreen();
			policyAggregateLimitPageObject.GetBackToPolicyScreen();
			Browser.refresh(driver);
			dashboardPageObject.getWindowForDashboard();
			dashboardPageObject.navigateToIRF();
			
			logger.log(LogStatus.INFO, "User navigated to Irf Screen");
			irfPageObjects.clickPolicyIcon();
			logger.log(LogStatus.INFO, "User clicked on Policy Icon");
			Wait.waitFor(5);
			
			irfPageObjects.selectPolicy(policyNum);
			logger.log(LogStatus.INFO, "User selects the created policy");

			logger.log(LogStatus.INFO, "System fetching 'Effective Date'");

			logger.log(LogStatus.INFO, "System fetching 'Expiration Date'");

			logger.log(LogStatus.INFO, "System fetching 'Carrier'");

			logger.log(LogStatus.INFO, "System fetching 'Client'");

			logger.log(LogStatus.INFO, "System fetching 'Policy Type'");

			String CustomClaimNumber = irfPageObjects.enterCustomClaimNumber();
			logger.log(LogStatus.INFO, "User enters on Custom Claim Number");
			irfPageObjects.enterIncidentDate(list.get(0));
			logger.log(LogStatus.INFO, "User enters the Incident Date");
			Wait.waitFor(10);
			irfPageObjects.clickSubmitButton();
			logger.log(LogStatus.INFO, "User clicks on Submit Button");
			irfPageObjects.clickContinueButton();
			irfPageObjects.closeIrfScreen();
			dashboardPageObject.getBackToParentWindow();
			searchClaimPageObjects = dashboardPageObject.navigateToClaimSearch();
			logger.log(LogStatus.INFO, "User navigated to Claim Search Screen");
			String claimNumber = carrierCode + policyCode + list.get(5) + CustomClaimNumber;
			searchClaimPageObjects.enterClaimNumber(claimNumber);
			logger.log(LogStatus.INFO, "User enters the Claim Number");
			searchClaimPageObjects.selectSearch();
			logger.log(LogStatus.INFO, "User clicks on Search Button");
			Wait.waitFor(5);
			searchClaimPageObjects.selectClaimNumber();
			logger.log(LogStatus.INFO, "User selects the claim number");
			Wait.waitFor(5);
			claimScreenPageObjects.selectClaimType();
			Wait.waitFor(2);
			logger.log(LogStatus.INFO, "User change the claim Type to Active");
			claimScreenPageObjects.selectClaimStatus();
			logger.log(LogStatus.INFO, "User change the claim status to Open");
			Wait.waitFor(2);
			claimScreenPageObjects.selectAdjuster();
			logger.log(LogStatus.INFO, "User selects the Adjuster");
			claimScreenPageObjects.selectSupervisor();
			logger.log(LogStatus.INFO, "User selects the Supervisor");
			claimScreenPageObjects.enterPolicyYear();
			logger.log(LogStatus.INFO, "User enters the Policy Year");
			claimScreenPageObjects.enterClaimAddress1();
			logger.log(LogStatus.INFO, "User enters the Claim Address1");
			claimScreenPageObjects.enterClaimZipCode();
			logger.log(LogStatus.INFO, "User enters the Zip Code, City & State");
			claimScreenPageObjects.enterIncidentDescription();
			logger.log(LogStatus.INFO, "User enters the Incident Description");
			claimScreenPageObjects.enterClaimDamages();
			logger.log(LogStatus.INFO, "User enters the Claim damages");
			
			logger.log(LogStatus.INFO, "User entered configured required fields");
			claimScreenPageObjects.clickOnSave();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(7);
			
			claimantScreenPageObjects.clickClaimantTab();
			logger.log(LogStatus.INFO, "User is directed to claimant Screen");
			Wait.waitFor(8);
			claimantScreenPageObjects.selectClaimantType();
			Wait.waitFor(2);
			logger.log(LogStatus.INFO, "User selects the claimant Type as Claimant");
			claimantScreenPageObjects.clickAddClaimantButton();
			logger.log(LogStatus.INFO, "User clicks on Add Claimant Button");
			Wait.waitFor(2);
			claimantScreenPageObjects.addClaimantName();
			logger.log(LogStatus.INFO, "User enters the claimant Name");
			Wait.waitFor(2);
			claimantScreenPageObjects.addClaimantAddress();
			Wait.waitFor(2);
			logger.log(LogStatus.INFO, "User enters  the claimant Address");
			claimantScreenPageObjects.addClaimantZip();
			Wait.waitFor(5);
			System.out.println(claimantScreenPageObjects.getIncidentDateFromHeader());
			logger.log(LogStatus.INFO, "User enters  the claimant Zip");
			claimantScreenPageObjects.addReportDateToInsured();
			Wait.waitFor(2);
			String expectedReportDate=claimantScreenPageObjects.fetchReportDateToInsured();
			logger.log(LogStatus.INFO, "User enters  the Report Date To Insured Zip");
			claimantScreenPageObjects.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(3);
			subClaimPageObjects.clickSubClaimTab();
			logger.log(LogStatus.INFO, "User is directed to Sub Claim Screen");
			Wait.waitFor(8);
			subClaimPageObjects.clicksOnAddSubClaimButton();
			Wait.waitFor(4);
			subClaimPageObjects.addInsurableItem();
			logger.log(LogStatus.INFO, "User has added Insurable Item");
			Wait.waitFor(2);
			subClaimPageObjects.addCoverageItem();
			logger.log(LogStatus.INFO, "User has added Covarage ");
			subClaimPageObjects.addLossType();
			logger.log(LogStatus.INFO, "User has added Loss Type");
			subClaimPageObjects.clickSaveButton();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(3);
			dashboardPageObject.clickReserve();
			logger.log(LogStatus.INFO, "Click the reserve tab");
			Wait.waitFor(2);
			reservePageObject.getWindowForReserveScreen();
			Wait.waitFor(2);
			reservePageObject.clickWorkSheet();
			logger.log(LogStatus.INFO, "Click on worksheet button");
			Wait.waitFor(2);
			reservePageObject.switchToWorkSheetScreen();
			Wait.waitFor(2);
			System.out.println(driver.getTitle());
			logger.log(LogStatus.INFO, "Clicks on worksheet button");
			Wait.waitFor(3);
			reservePageObject.getWindowForReserveWorksheet();
			String enteredReserveAmountForAllocatedExpense = reservePageObject.enterNewReserves();
			String enteredReserveAmountForLoss = reservePageObject.enterNewReservesForLoss();
			String enteredReserveAmountForIAExpense = reservePageObject.enterNewReservesForIAExpense();
			logger.log(LogStatus.INFO, "Enter the reserve amountfor all categories");
			reservePageObject.enterReasonCode();
			reservePageObject.enterReasonCodeForLoss();
			reservePageObject.enterReasonCodeForIAExpense();
			logger.log(LogStatus.INFO, "Select the reason code from drop down");
			
			Wait.waitFor(2);
			reservePageObject.clickPostChangesButton();
			logger.log(LogStatus.INFO, "Click on post changes button");
			Wait.waitFor(5);
			reservePageObject.switchToReserveNotesWindow();
			reservePageObject.closeWindow();
			Wait.waitFor(3);
//			reservePageObject.switchToWorkSheetScreenAgain();
//			Wait.waitFor(3);
//			reservePageObject.closeWindow();
//			Wait.waitFor(3);
			reservePageObject.switchToReserveScreenAgain();
			Wait.waitFor(2);
			reservePageObject.refresh();
			Wait.waitFor(2);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
			policySetupPageObject.clickSearchButton();
			logger.log(LogStatus.INFO, "Search the policy");
			policySetupPageObject.goToLastSearchResultScreen();
			Wait.waitFor(10);
			policySetupPageObject.fetchLatestPolicyNumber();
			policyAggregateLimitPageObject.clickOnAggregaeIcon();
			logger.log(LogStatus.INFO, "Click on Aggregate Icon");
			bc.switchToNextWindow("Non Comp Policy Aggregate Limits");
			String incurredAmount=policyAggregateLimitPageObject.fetchIncurredAmount();
			Assert.assertEquals(incurredAmount, "$"+enteredReserveAmountForLoss+".00");
			logger.log(LogStatus.PASS, "Incurred Amount for Aggregate is verified");
			
			

		
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}
	
	

}
