package utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeScreenshot {

	// Description: used to capture fail screen shots at 'failScreen' folder
	// Parameter1:testCaseName(name of current test cases that being executed)
	// Parameter2: driver(WebDriver instance)
	public static void takeFailScreen(String testCaseName, WebDriver driver) throws IOException {
		// used to capture fail screens
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// The below method will save the screen shot in d drive with name
		// "screenshot.png"
		FileUtils.copyFile(scrFile, new File(Configuration.failSceenLocation + testCaseName + ".png"));

	}

	// Description: used to capture normal screen shots at 'normalScreen' folder
	// Parameter1:testCaseName(name of current test cases that being executed)
	// Parameter2: driver(WebDriver instance)
	

}
