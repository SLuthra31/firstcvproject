package utilities;

import com.relevantcodes.extentreports.ExtentReports;

public class ReportManager {
	 private static ExtentReports extent;
	    public synchronized static ExtentReports getReporter() {
	        if (extent == null) {
	            extent = new ExtentReports(Configuration.reportPath, true);
	            
	            extent
	                .addSystemInfo("Host Name", "PCIS")
	                .addSystemInfo("Environment", "QA");
	        }
	        
	        return extent;
	    }
	}


