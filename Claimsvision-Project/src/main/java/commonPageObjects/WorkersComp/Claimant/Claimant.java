package commonPageObjects.WorkersComp.Claimant;

import java.io.IOException;
import java.text.ParseException;
import java.util.Random;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.ExcelReader;
import utilities.Wait;

public class Claimant {

	WebDriver driver;

	public Claimant(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "input#saveClaimImageButton")
	public WebElement saveIcon;
	
	@FindBy(css = "input#closeClaimImageButton")
	public WebElement closeClaimIcon;
	
	@FindBy(css = "input#openClaimImageButton")
	public WebElement openClaimIcon;
	
	@FindBy(css = "input#ClaimantAccident_claimNumberTextBox")
	public WebElement claimNumber;

	@FindBy(css = "select#ClaimantAccident_policyTypeDropDown")
	public WebElement policyType;

	@FindBy(css = "select#ClaimantAccident_coverageDropDown")
	public WebElement coverage;

	@FindBy(css = "input#ClaimantAccident_claimantFirstNameTextBox")
	public WebElement claimantFirstName;

	@FindBy(css = "input#ClaimantAccident_claimantMiddleNameTextBox")
	public WebElement claimantMiddleName;

	@FindBy(css = "input#ClaimantAccident_claimantLastNameTextBox")
	public WebElement claimantlastName;

	@FindBy(css = "input#ClaimantAccident_claimantSuffixTextBox")
	public WebElement suffix;

	@FindBy(css = "input#ClaimantAccident_emailTextBox")
	public WebElement emailAddress;

	@FindBy(css = "input#ClaimantAccident_ClaimantPreferredNameTextBox")
	public WebElement prefferedName;

	@FindBy(css = "input#ClaimantAccident_uniqueISOClaimTextBox")
	public WebElement uniqueISOClaim;

	@FindBy(css = "input#ClaimantAccident_socialSecurityTextBox")
	public WebElement claimantID;
	
	@FindBy(css="select#ClaimantAccident_IdTypeDropDown")
	public WebElement idType;

	@FindBy(css = "input#ClaimantAccident_employeeIDTextBox")
	public WebElement employeeID;

	@FindBy(css = "input#ClaimantAccident_PolicyNumberTextBox")
	public WebElement policyNumber;

	@FindBy(css = "input#ClaimantAccident_ssnReleaseCheckBox")
	public WebElement ssnRelease;

	@FindBy(css = "input#ClaimantAccident_medicalRecordReleaseCheckBox")
	public WebElement medicalRecordRelease;

	@FindBy(css = "input#ClaimantAccident_foreignAddressCheckBox")
	public WebElement foreignAddressCheckBox;

	@FindBy(css = "input#ClaimantAccident_claimantAddress1TextBox")
	public WebElement claimantAddress1;

	@FindBy(css = "input#ClaimantAccident_claimantHomePhoneTextBox")
	public WebElement homePhone;

	@FindBy(css = "input#ClaimantAccident_claimantAddress2TextBox")
	public WebElement claimantAddress2;

	@FindBy(css = "input#ClaimantAccident_cellPhoneTextBox")
	public WebElement cellPhone;

	@FindBy(css = "select#ClaimantAccident_statusDropDown")
	public WebElement status;

	@FindBy(css = "select#ClaimantAccident_claimTypeDropDown1")
	public WebElement claimType;

	@FindBy(css = "input#ClaimantAccident_cityTextBox")
	public WebElement city;

	@FindBy(css = "select#ClaimantAccident_stateDropDown")
	public WebElement state;

	@FindBy(css = "input#ClaimantAccident_zipTextBox")
	public WebElement zip;

	@FindBy(css = "input#ClaimantAccident_countyTextBox")
	public WebElement county;

	@FindBy(css = "input#ClaimantAccident_compensableRadioList_0")
	public WebElement compensableYes;

	@FindBy(css = "input#ClaimantAccident_claimDeniedRadioList_1")
	public WebElement compensableNo;

	@FindBy(css = "select#ClaimantAccident_closedReasonDropDown")
	public WebElement closedReason;

	@FindBy(css = "input#ClaimantAccident_clientTextBox")
	public WebElement client;

	@FindBy(css = "img#ClaimantAccident_searchClientImageButton")
	public WebElement clientSearchIcon;

	@FindBy(css = "input#ClaimantAccident_accidentLocationTextBox")
	public WebElement levelAssignedTo;

	@FindBy(css = "img#ClaimantAccident_searchLocationImage")
	public WebElement levelAssignedToSearchIcon;

	@FindBy(css = "select#ClaimantAccident_adjusterDropDown")
	public WebElement adjuster;

	@FindBy(css = "select#ClaimantAccident_supervisorDropDown")
	public WebElement supervisor;

	@FindBy(css = "input#ClaimantAccident_claimDeniedRadioList_0")
	public WebElement claimDeniedYes;

	@FindBy(css = "input#ClaimantAccident_claimDeniedRadioList_1")
	public WebElement claimDeniedNo;

	@FindBy(css = "input#ClaimantAccident_dateDeniedTextBox")
	public WebElement dateDenied;

	@FindBy(css = "input#ClaimantAccident_denialRescindedCheckBox")
	public WebElement denialRescinded;

	@FindBy(css = "input#ClaimantAccident_denialRescissionDateTextBox")
	public WebElement denialRescindedDate;

	@FindBy(css = "select#ClaimantAccident_claimConversionStatusDropdownList")
	public WebElement claimConversionStatus;

	@FindBy(css = "select#ClaimantAccident_DenialTypeDropDownList")
	public WebElement denialType;

	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList")
	public WebElement denialReason1;

	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList2")
	public WebElement denialReason2;

	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList3")
	public WebElement denialReason3;

	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList4")
	public WebElement denialReason4;

	@FindBy(css = "input#ClaimantAccident_denialReasonDropDownList5")
	public WebElement denialReason5;

	@FindBy(css = "input#ClaimantAccident_sexRadioList_0")
	public WebElement sexMale;

	@FindBy(css = "input#ClaimantAccident_sexRadioList_1")
	public WebElement sexFemale;

	@FindBy(css = "select#ClaimantAccident_maritalStatusDropDown")
	public WebElement maritalStatus;

	@FindBy(css = "input#ClaimantAccident_birthTextBox")
	public WebElement dob;
	@FindBy(css = "input#ClaimantAccident_ageOnInjuryDateTextBox")
	public WebElement age;

	@FindBy(css = "input#ClaimantAccident_numberOfDependentsTextBox")
	public WebElement numberOfDependents;

	@FindBy(css = "input#ClaimantAccident_injuryDateTextBox")
	public WebElement dateOfIncident;
	
	@FindBy(css= "select#ClaimantAccident_injuryTimeControl_hourDropDown")
	public WebElement hourDropdown;
	
	@FindBy(css= "select#ClaimantAccident_injuryTimeControl_minuteDropDown")
	public WebElement minutesDropdown;
	
	@FindBy(css= "select#ClaimantAccident_injuryTimeControl_meridianDropDown")
	public WebElement meridianDropdown;

	@FindBy(css = "input#ClaimantAccident_reportDateToERTextBox")
	public WebElement dateOfKnowledge;

	@FindBy(css = "input#ClaimantAccident_entryDateTextBox")
	public WebElement entryDate;

	@FindBy(css = "input#ClaimantAccident_accidentDescriptionTextBox")
	public WebElement incidentDescription;

	@FindBy(css = "select#ClaimantAccident_majorCauseDropDown")
	public WebElement causeOfIncidentMajor;

	@FindBy(css = "select#ClaimantAccident_minorCauseDropDown")
	public WebElement causeOfIncidentMinor;

	@FindBy(css = "select#ClaimantAccident_majorNatureDropDown")
	public WebElement natureOfIncidentMajor;

	@FindBy(css = "select#ClaimantAccident_minorNatureDropDown")
	public WebElement natureOfIncidentMinor;

	@FindBy(css = "select#ClaimantAccident_majorPartDropDown")
	public WebElement partOfBodyMajor;

	@FindBy(css = "select#ClaimantAccident_minorPartDropDown")
	public WebElement partOfBodyMinor;

	@FindBy(css = "input#ClaimantAccident_PtOfImpTextBox")
	public WebElement percentOfImpairement;

	@FindBy(css = "textarea#ClaimantAccident_claimSummaryTextBox")
	public WebElement claimSummary;

	@FindBy(css = "select#ClaimantAccident_fatalityDropDownList")
	public WebElement fatality;

	@FindBy(css = "input#ClaimantAccident_fatalityDateTextBox")
	public WebElement deathDate;

	@FindBy(css = "input#ClaimantAccident_OccurrenceRadioList_0")
	public WebElement incidentRelatedToOccuerenceYes;

	@FindBy(css = "input#ClaimantAccident_OccurrenceRadioList_1")
	public WebElement incidentRelatedToOccuerenceMo;

	@FindBy(css = "input#ClaimantAccident_occurrenceNumberTextBox")
	public WebElement occurenceNumber;

	@FindBy(css = "input#ClaimantAccident_initiallyDateTextBox")
	public WebElement dateInitiallyReportedToXS;

	@FindBy(css = "input#ClaimantAccident_lastDateTextBox")
	public WebElement dateLastReportedToXS;
	
	@FindBy(css = "input#ClaimantAccident_ClaimantMedicareControl_hicnNumberTextBox")
	public WebElement hicnNumber;

	@FindBy(css = "select#ClaimantAccident_ClaimantMedicareControl_dispositionCodeDropDown")
	public WebElement claimDisposition;

	@FindBy(css = "select#ClaimantAccident_ClaimantMedicareControl_queryDispositionDropDown")
	public WebElement queryDisposition;

	@FindBy(css = "span#ClaimantAccident_ClaimantMedicareControl_ormIndicatorList")
	public WebElement ormIndicator;

	@FindBy(css = "input#ClaimantAccident_ClaimantMedicareControl_ormDateTextBox")
	public WebElement ormTerminationDate;

	@FindBy(css = "input#ClaimantAccident_firstExposureTextBox")
	public WebElement firstExposureDate;

	@FindBy(css = "input#ClaimantAccident_privacyRadioButtonList_0")
	public WebElement claimPrivacyYes;

	@FindBy(css = "input#ClaimantAccident_adminReportedDateTextBox")
	public WebElement dateReportedToClaimAdmin;

	@FindBy(css = "input#ClaimantAccident_stateClaimNumberTextBox")
	public WebElement stateClaimNumber;

	@FindBy(css = "input#ClaimantAccident_isAcquiredClaimCheckBox")
	public WebElement acquiredClaimNumber;

	@FindBy(css = "input#ClaimantAccident_isHoldExtractCheckBox")
	public WebElement holdExtract;

	@FindBy(css = "input#ClaimantAccident_isContinuousTraumaCheckBox")
	public WebElement continousTrauma;

	@FindBy(css = "input#ClaimantAccident_isAgreementToCompensateCheckBox")
	public WebElement aggrementToCompensate;

	@FindBy(css = "input#ClaimantAccident_isIsoExcludeCheckBox")
	public WebElement isoExclude;

	@FindBy(css = "select#ClaimantAccident_FROILateReasonDropDownList")
	public WebElement froiLateReasonCode;

	@FindBy(css = "input#ClaimantAccident_CompLastExposureTextBox")
	public WebElement lastExposureDate;

	@FindBy(css = "input#ClaimantAccident_claimSuspendedCheckBox")
	public WebElement claimSusupended;
	
	@FindBy(css = "select#ClaimantAccident_suspensionLevelDropDown")
	public WebElement suspensionLevel;
	
	@FindBy(css = "select#ClaimantAccident_suspensionTypeDropDown")
	public WebElement suspensionType;
	
	@FindBy(css = "input#ClaimantAccident_suspensionEffectiveDateTextBox")
	public WebElement suspensionEffDate;
	
	@FindBy(css = "input#ClaimantAccident_suspensionNarrativeTextBox")
	public WebElement suspensionNarrative;
	
	@FindBy(css = "input#ClaimantAccident_suspensionRescindedCheckBox")
	public WebElement suspensionRescinded;
	
	@FindBy(css = "input#ClaimantAccident_RescissionDateTextBox")
	public WebElement rescissionDate;
	
	@FindBy(css = "input#ClaimantAccident_saveButton")
	public WebElement save;

	@FindBy(css = "input#ClaimantAccident_cancelButton")
	public WebElement cancel;
	
	@FindBy(css = "span#ClaimantAccident_saveMessageLabel")
	public WebElement validationMessage;
	
	public String getValueOfClaimNumber(){
		String claimNum = claimNumber.getText();
		return claimNum;
	}
	
	public String getValueOfDoi(){
		String doi = dateOfIncident.getText();
		return doi;
	}
	
	public String getDateReported(){
		String dateReported = dateReportedToClaimAdmin.getText();
		return dateReported;
	}
	
	public String getClaimantFirstName(){
		String claimantFName = claimantFirstName.getText();
		return claimantFName;
	}
	
	public String getClaimantMiddleName(){
		String claimantMName = claimantMiddleName.getText();
		return claimantMName;
	}
		
	public String getClaimantLastName(){
		String claimantLName = claimantlastName.getText();
		return claimantLName;
	}
	
	public String getClaimStatus(){
		Select s2 = new Select(status);
		String claimStatus = s2.getFirstSelectedOption().getText();
		return claimStatus;
	}
	
	public String getDateOfBirth(){
		String dateOfBirth = dob.getText();
		return dateOfBirth;
	}
	
	public String getValueOfSSN(){
		String ssnNo = claimantID.getText();
		return ssnNo;
	}
	
	public String getValueOfEntryDate(){
		String entryDateValue = entryDate.getText();
		return entryDateValue;
	}
	
	
	public String getStateClaimNum(){
		String stateClaimNum = stateClaimNumber.getText();
		return stateClaimNum;
	}
	
	public String getIncidentDesc(){
		String incidentDesc = incidentDescription.getText();
		return incidentDesc;
	}
	
	public String getExaminer(){
		Select s2 = new Select(adjuster);
		String examiner = s2.getFirstSelectedOption().getText();
		return examiner;
	}
		
	public String getSupervisor(){
		Select s2 = new Select(supervisor);
		String supValue = s2.getFirstSelectedOption().getText();
		return supValue;
	}
	
	public String getClaimantAddress1(){
		String address1 = claimantAddress1.getText();
		return address1;
	}
	
	
	public String getClaimantAddress2(){
		String address2 = claimantAddress2.getText();
		return address2;
	}
	
	public String getCity(){
		String cityValue = city.getText();
		return cityValue;
	}
	
	public String getState(){
		Select s2 = new Select(state);
		String stateValue = s2.getFirstSelectedOption().getText();
		return stateValue;
	}
	
	public String getZip(){
		String zipValue = zip.getText();
		return zipValue;
	}
	
	public String getCellPhone(){
		String phoneNumber = cellPhone.getText();
		return phoneNumber;
	}
	
	public String getAgeOnDOI(){
		String ageValue = age.getText();
		return ageValue;
	}
	
	public String getMaritalStatus(){
		Select s =new Select(maritalStatus);
		String maritalStatusValue = s.getFirstSelectedOption().getText();
		return maritalStatusValue;
	}
	
	public String getNumOfDepts(){
		String numOfDept = numberOfDependents.getText();
		return numOfDept;
	}
	
	public String getPolicyType(){
		Select s =new Select(policyType);
		String polType = s.getFirstSelectedOption().getText();
		return polType;
	}
	
	public void saveValuesFromClaimants() throws Exception{
		String claimNum = claimNumber.getText();
		ExcelReader.setCell(2, 12, 0, claimNum);
		
		String doi = dateOfIncident.getText();
		ExcelReader.setCell(2, 12, 1, doi);
		
		String dateReported = dateReportedToClaimAdmin.getText();
		ExcelReader.setCell(2, 12, 3, dateReported);
		
	
	/*	
		Select s1 = new Select(coverage);
		String cov = s1.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 1, 2, cov);*/
		
		String claimantFName = claimantFirstName.getText();
		String claimantMName = claimantMiddleName.getText();
		String claimantLName = claimantlastName.getText();
		ExcelReader.setCell(2, 12, 4, claimantFName + ""+ claimantMName + "" + claimantLName);
		
		Select s2 = new Select(status);
		String claimStatus = s2.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 12, 5, claimStatus);
		
		String dateOfBirth = dob.getText();
		ExcelReader.setCell(2, 12, 6, dateOfBirth);
		
		String ssn = claimantID.getText();
		ExcelReader.setCell(2, 12, 7, ssn);
		
		String eDate = entryDate.getText();
		ExcelReader.setCell(2, 12, 8, eDate);
		
		String stateClaim = stateClaimNumber.getText();
		ExcelReader.setCell(2, 12, 11, stateClaim);
		
		String incidentDesc = incidentDescription.getText();
		ExcelReader.setCell(2, 12, 12, incidentDesc);
		
		Select s3 = new Select(adjuster);
		String examiner = s3.getFirstSelectedOption().getText(); 
		ExcelReader.setCell(2, 12, 13, examiner);
		
		Select s4 = new Select(supervisor);
		String supervr = s4.getFirstSelectedOption().getText(); 
		ExcelReader.setCell(2, 12, 14, supervr);
		
		String address1 = claimantAddress1.getText();
		ExcelReader.setCell(2, 12, 15, address1);
		
		String address2 = claimantAddress2.getText();
		ExcelReader.setCell(2, 12, 16, address2);
		
		String cityvalue = city.getText();
		ExcelReader.setCell(2, 12, 17, cityvalue);
		
		Select s5 = new Select(state);
		String statevalue = s5.getFirstSelectedOption().getText(); 
		ExcelReader.setCell(2, 12, 18, statevalue);
		
		String zipvalue = zip.getText();
		ExcelReader.setCell(2, 12, 19, zipvalue);
		
		if(!foreignAddressCheckBox.isSelected()){
			ExcelReader.setCell(2, 12, 20, "US");
		}
		
		String phoneNumber = cellPhone.getText();
		ExcelReader.setCell(2, 12, 21, phoneNumber);
		
		String ageValue = age.getText();
		ExcelReader.setCell(2, 12, 22, ageValue);
		
		Select s6 = new Select(maritalStatus);
		String maritalStatusvalue = s6.getFirstSelectedOption().getText(); 
		ExcelReader.setCell(2, 12, 23, maritalStatusvalue);
		
		String numOfDept = numberOfDependents.getText();
		ExcelReader.setCell(2, 12, 24, numOfDept);
		
		Select s = new Select(policyType);
		String polType = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 12, 25, polType);
		
		String hicn = hicnNumber.getText();
		ExcelReader.setCell(2, 12, 29, hicn);
		
		Select s7 = new Select(claimDisposition);
		String claimDisp = s7.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 12, 30, claimDisp);
		
		Select s8 = new Select(queryDisposition);
		String queryDisp = s8.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 12, 31, queryDisp);
		
		if(!ormTerminationDate.isDisplayed()){
			ExcelReader.setCell(2, 12, 32, "N");
		}
		else{
			ExcelReader.setCell(2, 12, 32, "Y");
			ExcelReader.setCell(2, 12, 33, ormTerminationDate.getText());
		}
		
		Select s9 = new Select(fatality);
		String deathResult = s9.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 12, 34, deathResult);
		
		String deathDateValue = deathDate.getText();
		ExcelReader.setCell(2, 12, 35, deathDateValue);
		
		
	}
	
	public  void setClaimantName() throws Exception{
		Wait.waitFor(5);
		claimantFirstName.clear(); 
		claimantFirstName.sendKeys(BaseClass.stringGeneratorl(7));
		claimantMiddleName.clear(); 
		claimantMiddleName.sendKeys(BaseClass.stringGeneratorl(1));
		claimantlastName.clear(); 
		claimantlastName.sendKeys(BaseClass.stringGeneratorl(7));
		suffix.clear();
		suffix.sendKeys(BaseClass.stringGeneratorl(3));
		emailAddress.clear();
		emailAddress.sendKeys("test"+BaseClass.enterRandomNumber(2)+"@pcisvision.com");
		prefferedName.clear();
		prefferedName.sendKeys(BaseClass.stringGeneratorl(5));
		uniqueISOClaim.clear();
		uniqueISOClaim.sendKeys(BaseClass.stringGeneratorl(5));
		claimantID.clear();
		claimantID.sendKeys(BaseClass.enterRandomNumber(10));
		Select s=new Select(idType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		employeeID.clear();
		employeeID.sendKeys(BaseClass.enterRandomNumber(8));
		if(!ssnRelease.isSelected()){
			ssnRelease.click();
		}
		if(!medicalRecordRelease.isSelected()){
			medicalRecordRelease.click();
		}
		
		
	}	
	
	public void clickOnSaveButton(){
		save.click();
	}
	/*	
	public  void setClaimantMiddleName(){	
		 claimantMName = claimantMiddleName.sendKeys();
		 claimantMName;
	}	
		
	public  void setClaimantLastName(){	
		 claimantLName = claimantlastName.sendKeys();
		 claimantLName;
	}	
		
	public  void setClaimStatus(){	
		Select s2 = new Select(status);
		 claimStatus = s2.void setFirstSelectedOption().sendKeys();
		 claimStatus;
	}	
		
	public  void setDateOfBirth(){	
		 dateOfBirth = dob.sendKeys();
		 dateOfBirth;
	}	
		
	public  void setValueOfSSN(){	
		 ssnNo = claimantID.sendKeys();
		 ssnNo;
	}	
		
	public  void setValueOfEntryDate(){	
		 entryDateValue = entryDate.sendKeys();
		 entryDateValue;
	}	
		
		
	public  void setStateClaimNum(){	
		 stateClaimNum = stateClaimNumber.sendKeys();
		 stateClaimNum;
	}	
		
	public  void setIncidentDesc(){	
		 incidentDesc = incidentDescription.sendKeys();
		 incidentDesc;
	}	
		
	public  void setExaminer(){	
		Select s2 = new Select(adjuster);
		 examiner = s2.void setFirstSelectedOption().sendKeys();
		 examiner;
	}	
		
	public  void setSupervisor(){	
		Select s2 = new Select(supervisor);
		 supValue = s2.void setFirstSelectedOption().sendKeys();
		 supValue;
	}	
		
	public  void setClaimantAddress1(){	
		 address1 = claimantAddress1.sendKeys();
		 address1;
	}	
		
		
	public  void setClaimantAddress2(){	
		 address2 = claimantAddress2.sendKeys();
		 address2;
	}	
		
	public  void setCity(){	
		 cityValue = city.sendKeys();
		 cityValue;
	}	
		
	public  void setState(){	
		Select s2 = new Select(state);
		 stateValue = s2.void setFirstSelectedOption().sendKeys();
		 stateValue;
	}	
		
	public  void setZip(){	
		 zipValue = zip.sendKeys();
		 zipValue;
	}	
		
	public  void setCellPhone(){	
		 phoneNumber = cellPhone.sendKeys();
		 phoneNumber;
	}	
		
	public  void setAgeOnDOI(){	
		 ageValue = age.sendKeys();
		 ageValue;
	}	
		
	public  void setMaritalStatus(){	
		Select s =new Select(maritalStatus);
		 maritalStatusValue = s.void setFirstSelectedOption().sendKeys();
		 maritalStatusValue;
	}	
		
	public  void setNumOfDepts(){	
		 numOfDept = numberOfDependents.sendKeys();
		 numOfDept;
	}	
		
	public  void setPolicyType(){	
		Select s =new Select(policyType);
		 polType = s.void setFirstSelectedOption().sendKeys();
		 polType;
	}	
*/
	public void enterClaimantFirstName(){
		claimantFirstName.clear(); 
		claimantFirstName.sendKeys(BaseClass.stringGeneratorl(7));
	}

	public void enterClaimantMiddleName(){
		claimantMiddleName.clear(); 
		claimantMiddleName.sendKeys(BaseClass.stringGeneratorl(1));
	}
	
	public void enterClaimantLastName(){
		claimantlastName.clear(); 
		claimantlastName.sendKeys(BaseClass.stringGeneratorl(7));
	}
	
	public void enterClaimantSuffix(){
		suffix.clear();
		suffix.sendKeys(BaseClass.stringGeneratorl(3));
	}
	
	public void enterClaimantEmailAddress(){
		emailAddress.clear();
		emailAddress.sendKeys("test"+BaseClass.enterRandomNumber(2)+"@pcisvision.com");
	}
	
	public void enterClaimantPreferredName(){
		prefferedName.clear();
		prefferedName.sendKeys(BaseClass.stringGeneratorl(5));
	}
	
	public void enterUniqueISOClaim(){
		uniqueISOClaim.clear();
		uniqueISOClaim.sendKeys(BaseClass.stringGeneratorl(5));
	}
	
	public void enterClaimantID(){
		claimantID.clear();
		claimantID.sendKeys(BaseClass.enterRandomNumber(10));
	}
	
	public void selectClaimantIdType(){
		Select s=new Select(idType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterEmployeeID(){
		employeeID.clear();
		employeeID.sendKeys(BaseClass.enterRandomNumber(8));
	}
	
	public void selectSSNRelease(){
		if(!ssnRelease.isSelected()){
			ssnRelease.click();
			
		}
	}
	
	public void selectMedicalRecordRelease(){
		if(!medicalRecordRelease.isSelected()){
			medicalRecordRelease.click();
			
		}
	}
	
	public void selectClaimType(){
		Select s1 = new Select(claimType);
		s1.selectByIndex(BaseClass.generateRandomNumberAsInteger(s1.getOptions().size()));
	}
	
	public void enterClaimantAddress1(){
		claimantAddress1.clear();
		claimantAddress1.sendKeys(BaseClass.stringGeneratorl(5)+" "+BaseClass.stringGeneratorl(4));
		
	}
	
	public void enterClaimantAddress2(){
		claimantAddress2.clear();
		claimantAddress2.sendKeys(BaseClass.stringGeneratorl(5)+" "+BaseClass.stringGeneratorl(4));
	}
	
	public void enterHomePhone(){
		homePhone.clear();
		homePhone.sendKeys(BaseClass.enterRandomNumber(10));
	}
	
	public void enterCellPhone(){
		cellPhone.clear();
		cellPhone.sendKeys(BaseClass.enterRandomNumber(10));
	}
	
	public void selectStatus(){
		Select s2 = new Select(status);
		s2.selectByIndex(BaseClass.generateRandomNumberAsInteger(s2.getOptions().size()));
	}
	
	public void selectCompensable() throws InterruptedException, IOException {

		BaseClass.clickRandomWebElement(compensableNo, compensableYes);
	}
	
	public void selectAdjuster(){
		Select s3 = new Select(adjuster);
		s3.selectByIndex(BaseClass.generateRandomNumberAsInteger(s3.getOptions().size()));
	}
	
	public void selectSupervisor(){
		Select s3 = new Select(supervisor);
		s3.selectByIndex(BaseClass.generateRandomNumberAsInteger(s3.getOptions().size()));
	}
	
	public void selectConversionStatus(){
		
		Select s3 = new Select(claimConversionStatus);
		if(s3.getOptions().size()>1){
		s3.selectByIndex(BaseClass.generateRandomNumberAsInteger(s3.getOptions().size()));
		}
	}
	
	public void selectSex() throws InterruptedException, IOException {

		BaseClass.clickRandomWebElement(sexMale, sexFemale);
	}
	
	public void selectMaritalStatus(){
		Select s = new Select(maritalStatus);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterDateOfBirth(String date) throws InterruptedException, ParseException, IOException {
		Wait.modifyWait(driver, dob);
		String dateOfBirthValue = BaseClass.generateDateLessThanSpecificDate(date);
		dob.sendKeys(dateOfBirthValue);
		ExcelReader.setCell(1, 9, 11, dateOfBirthValue);
	}
	
	public void enterNumOfDepts(){
		numberOfDependents.clear();
		numberOfDependents.sendKeys(Integer.toString(BaseClass.generateRandomNumberAsInteger(10)));
	}
	
	public String enterIncidentDate() throws Exception {

		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,dateOfIncident);
		dateOfIncident.sendKeys(date);
		return date;		
	}
	
	public void selectTimeOfIncident(){
		Select s = new Select(hourDropdown);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		Select s1=new Select(minutesDropdown);
		s1.selectByIndex(BaseClass.generateRandomNumberAsInteger(s1.getOptions().size()));
		Select s2=new Select(meridianDropdown);
		s2.selectByIndex(BaseClass.generateRandomNumberAsInteger(s2.getOptions().size()));
	}
	
	public void enterDateOfKnowledge() throws Exception{
		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,dateOfIncident);
		dateOfIncident.sendKeys(date);
	}
	
	public void enterIncidentDesc(){
		incidentDescription.clear();
		incidentDescription.sendKeys(BaseClass.stringGeneratorl(15));
	}
	
	public void selectCauseOfIncidentMajor(){
		Select s= new Select(causeOfIncidentMajor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectCauseOfIncidentMinor(){
		Select s= new Select(causeOfIncidentMinor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectNatureOfIncidentMajor(){
		Select s= new Select(natureOfIncidentMajor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectNatureOfIncidentMinor(){
		Select s= new Select(natureOfIncidentMinor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectPartOfBodyMajor(){
		Select s= new Select(partOfBodyMajor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectPartOfBodyMinor(){
		Select s= new Select(partOfBodyMinor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterBodyPerCent() throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, percentOfImpairement);
		String bodyPartPercentValue = BaseClass.enterRandomNumber(2);
		percentOfImpairement.sendKeys(bodyPartPercentValue);
		ExcelReader.setCell(1, 9, 70, bodyPartPercentValue);
	}
	
	public void enterClaimSummary(){
		claimSummary.clear();
		claimSummary.sendKeys(BaseClass.stringGeneratorl(5)+" "+ BaseClass.stringGeneratorl(5));
	}
	
	public void selectFatalityReason(){
		Select s = new Select(fatality);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterDeathDate(String date) throws Exception{
		Wait.modifyWait(driver, deathDate);
		String deathDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
		deathDate.sendKeys(deathDateValue);
		ExcelReader.setCell(1, 9, 11, deathDateValue);
	}
	
	public void enterInitialReportedDate(String date) throws Exception{
		Wait.modifyWait(driver, dateInitiallyReportedToXS);
		String deathDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
		dateInitiallyReportedToXS.sendKeys(deathDateValue);
		ExcelReader.setCell(1, 9, 11, deathDateValue);
	
	}
	
	public void enterLastReportedDate(String date) throws Exception{
		Wait.modifyWait(driver, dateLastReportedToXS);
		String deathDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
		dateLastReportedToXS.sendKeys(deathDateValue);
		ExcelReader.setCell(1, 9, 11, deathDateValue);
		
	}
	
	public void enterHICNNumber(){
		hicnNumber.clear();
		hicnNumber.sendKeys(BaseClass.enterRandomNumber(10));
	}
	
	public void enterFirstExposureDate() throws Exception{
		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,firstExposureDate);
		firstExposureDate.sendKeys(date);
	}
	
	public void enterLastExposureDate() throws Exception{
		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,lastExposureDate);
		lastExposureDate.sendKeys(date);
	}
	
	public void selectAcquiredClaim(){
		if(!acquiredClaimNumber.isSelected()){
			acquiredClaimNumber.click();
		}
	}
	
	public void selectHoldExtract(){
		if(!holdExtract.isSelected()){
			holdExtract.click();
		}
	}
	
	public void selectContinuousTrauma(){
		if(!continousTrauma.isSelected()){
			continousTrauma.click();
		}
	}
	
	public void selectISOExclude(){
		if(!isoExclude.isSelected()){
			isoExclude.click();
		}
	}
	
	public void selectFROILateReason(){
		Select s = new Select(froiLateReasonCode);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectClaimSuspended(){
		if(!claimSusupended.isSelected()){
			claimSusupended.click();
		}
	}
	
	public void selectSuspensionLevel(){
		Select s = new Select(suspensionLevel);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectSuspensionType(){
		Select s = new Select(suspensionType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterSuspensionEffDate() throws Exception{
		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,suspensionEffDate);
		suspensionEffDate.sendKeys(date);
	}
	
	public void enterSuspensionNarrative(){
		suspensionNarrative.clear();
		suspensionNarrative.sendKeys(BaseClass.stringGeneratorl(10));
	}
	
	public void selectClaimRescinded(){
		if(!suspensionRescinded.isSelected()){
			suspensionRescinded.click();
		}
	}
	
	public void enterRescissionDate() throws Exception{
		Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		String day = Integer.toString(random.nextInt(30 - 10) + 10);
		String year = Integer.toString(random.nextInt(2017 - 1975) + 1975);
		String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver,rescissionDate);
		rescissionDate.sendKeys(date);
	}
	
	public void selectStatusOfClaimAsClosed(){
		Wait.waitFor(3);
		Select s=new Select(status);
		if(s.getFirstSelectedOption().getText().contains("Open")||s.getFirstSelectedOption().getText().contains("ReOpened")){
			s.selectByIndex(1);
		}
	}
	
	
	
	public void selectClosedReason(){
		Select s1= new Select(closedReason);
		s1.selectByIndex(BaseClass.generateRandomNumberAsInteger(s1.getOptions().size()));
	}
	
	public void clickOnSaveIcon(){
		Wait.waitFor(3);
		saveIcon.click();
	}
	
	public void clickOnCloseClaimIcon(){
		Wait.waitFor(3);
		closeClaimIcon.click();
		
		Alert alert=driver.switchTo().alert();
		alert.accept();
		Wait.waitFor(3);
	/*	Alert alert1=driver.switchTo().alert();
		alert1.accept();*/
		//driver.switchTo().parentFrame();
	
		
	}
	
	public void clickOnOpenClaimIcon(){
		Wait.waitFor(3);
		openClaimIcon.click();
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}
	
	public void handleAlertPopup(){
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}
}