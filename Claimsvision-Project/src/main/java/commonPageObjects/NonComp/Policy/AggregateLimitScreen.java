package commonPageObjects.NonComp.Policy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.Wait;

public class AggregateLimitScreen {

	WebDriver driver;
	BaseClass bc;

	public AggregateLimitScreen(WebDriver driver) {

		this.driver = driver;
		bc = new BaseClass(driver);

	}

	@FindBy(css = "img#AggregateLimitDetail_searchLocationImage")
	public WebElement clientIcon;

	@FindBy(css = "select#AggregateLimitDetail_policyTypeDropDown")
	public WebElement policyType;

	@FindBy(css = "select#AggregateLimitDetail_aggregateLimitDropDownList")
	public WebElement aggregateDropDown;

	@FindBy(css = "select#AggregateLimitDetail_coverageDropDown")
	public WebElement coverageDropDown;

	@FindBy(css = "input#conditionTextBox")
	public WebElement searchTextBox;

	@FindBy(css = "td#searchResultDataList_ctl01_TableCell1")
	public WebElement firstCell;

	@FindBy(css = "input#searchButton.defaultButton01")
	public WebElement searchButton;
	
	@FindBy(css = "input#AggregateLimitDetail_saveButton")
	public WebElement save;


	public void selectClientValueThroughText(String value) throws InterruptedException {
		Wait.modifyWait(driver, clientIcon);
		String parent = driver.getWindowHandle();
		clientIcon.click();
		Wait.waitFor(2);
		bc.switchToNextWindow("Search Client");
		Wait.waitFor(2);
		searchTextBox.sendKeys(value);
		Wait.waitFor(3);
		searchButton.click();
		Wait.waitFor(3);
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		driver.switchTo().window(parent);

	}

	public void selectPolicyType(String value) throws InterruptedException {
		Wait.modifyWait(driver, policyType);
		Select s = new Select(policyType);
		s.selectByVisibleText(value);

	}

	public void selectAggregateLimitCategory() throws InterruptedException {
		Wait.modifyWait(driver, aggregateDropDown);
		Select s = new Select(aggregateDropDown);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));

	}
	
	public void selectCoverage(String  coverage) throws InterruptedException {
		Wait.modifyWait(driver, coverageDropDown);
		Select s = new Select(coverageDropDown);
		s.selectByVisibleText(coverage);

	}
	
	public void selectSave() throws InterruptedException {
		Wait.modifyWait(driver, save);
		save.click();

	}


}
