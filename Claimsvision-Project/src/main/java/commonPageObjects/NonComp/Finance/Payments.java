package commonPageObjects.NonComp.Finance;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Wait;

public class Payments {

	WebDriver driver;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	public static String paymentFromDate = null;

	public Payments(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "table#ReservesForNonComp_ReservesGridView")
	public WebElement reserveGrid;

	@FindBy(css = "select#Payments_paymentTypeDropdown")
	public WebElement paymentType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_payeeTypeDropdownList")
	public WebElement payeeType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_modeTypeDropDown")
	public WebElement modeType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_bankModeDropDown")
	public WebElement paymentMode;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_fromDateTextBox")
	public WebElement paymentFrom;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_thruDateTextBox")
	public WebElement paymentThru;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_payCategoryDown")
	public WebElement payCat;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_payCategoryDown")
	public WebElement payCatForWC;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	public WebElement paySubCatForWC;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	public WebElement paySubCat;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_amountTextBox")
	public WebElement payAmt;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_amountTextBox")
	public WebElement payAmtForWC;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_checkDateTextBox")
	public WebElement checkDate;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_checkNumberTextBox")
	public WebElement checkNumber;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_saveNewButton1")
	public WebElement save;
	
	@FindBy(css = "span#Payments_ClaimPaymentOneTime_saveMessageLabel")
	public WebElement messageLabel;
	
	@FindBy(css = "input#Payments_ClaimPaymentOneTime_draftContinueButton")
	public WebElement draftButton;
	
	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ContinueButton")
	public WebElement continueButton;
	
	
	@FindBy(css = "table#Payments_PaymentDetailList")
	public WebElement paymentsTable;
	

	public void selectPaymentTypeAsOneTime() throws InterruptedException {
		Wait.modifyWait(driver, paymentType);
		Select s = new Select(paymentType);
		s.selectByVisibleText("One Time");

	}

	public void selectPayeeType() throws InterruptedException {
		Wait.modifyWait(driver, payeeType);
		Select s = new Select(payeeType);
		s.selectByVisibleText("Claimant");
	}

	public void selectModeType() throws InterruptedException {
		Wait.modifyWait(driver, modeType);
		Select s = new Select(modeType);
		// s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getAllSelectedOptions().size()
		// - 1));
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void selectPaymentMode() throws InterruptedException {
		Wait.modifyWait(driver, paymentMode);
		Select s = new Select(paymentMode);
		// s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getAllSelectedOptions().size()
		// - 1));
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void selectpayCat(String value) throws InterruptedException {
		Wait.modifyWait(driver, payCat);
		Select s = new Select(payCat);
		s.selectByVisibleText(value);

	}

	public void selectpayCatForWC(String value) throws InterruptedException {
		Wait.modifyWait(driver, payCatForWC);
		Select s = new Select(payCatForWC);
		s.selectByVisibleText(value);

	}

	public void enterPaymentFrom() throws InterruptedException {
		Wait.modifyWait(driver, paymentFrom);
		paymentFromDate = BaseClass.generatePaymentFromDate();
		paymentFrom.sendKeys(paymentFromDate);

	}

	public void enterPaymentThru() throws InterruptedException, ParseException {
		Wait.modifyWait(driver, paymentThru);
		String paymentThruDate = BaseClass.generateDateMoreThanSpecificDateByFewDays(paymentFromDate);
		paymentThru.sendKeys(paymentThruDate);

	}

	public void enterCheckDate() throws InterruptedException, ParseException {
		try {

			String checkDateValue = BaseClass.generateDateMoreThanSpecificDateByFewDays(paymentFromDate);
			checkDate.sendKeys(checkDateValue);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}

	}
	
	public void enterCheckDateForNonComp() throws InterruptedException, ParseException {
		try {

			String checkDateValue = BaseClass.generatePaymentFromDate();
			checkDate.sendKeys(checkDateValue);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}
	}

	public void enterCheckNumber() throws InterruptedException, ParseException {
		try {

			checkNumber.sendKeys(BaseClass.enterRandomNumber(5));
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}

	}

	public void selectpaySubCat(String value) throws InterruptedException {
		Wait.modifyWait(driver, paySubCat);
		Select s = new Select(paySubCat);
		s.selectByVisibleText(value);

	}

	public void selectpaySubCatForWC() throws InterruptedException {
		Wait.modifyWait(driver, paySubCatForWC);
		Select s = new Select(paySubCatForWC);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));

	}

	public String enterPayAmt() throws InterruptedException {
		Wait.modifyWait(driver, payAmt);
		String amt = BaseClass.enterRandomNumber(2);
		String finalAmount = "2" + amt;
		payAmt.sendKeys(finalAmount);
		return finalAmount;
	}

	public String enterPayAmtForWC() throws InterruptedException {
		Wait.modifyWait(driver, payAmtForWC);
		String amt = BaseClass.enterRandomNumber(2);
		String finalAmount = "2" + amt;
		payAmtForWC.sendKeys(finalAmount);
		return finalAmount;
	}

	public void clickSaveButton() throws InterruptedException {
		Wait.modifyWait(driver, save);
		save.click();

	}

	public String makeExpectedPaidAmount(float finalExpected) throws InterruptedException {
		{
			String finalExpectedAmount = Float.toString(finalExpected);

			if (finalExpectedAmount.substring(finalExpectedAmount.lastIndexOf(".")+1).length() < 2) {
				finalExpectedAmount = "$" + finalExpectedAmount + "0";
			}

			else {
				finalExpectedAmount = "$" + finalExpectedAmount;
			}

			return finalExpectedAmount;

		}

	}
	
	
	public String fetchMessage()
	{
		 return messageLabel.getText();
	}
	
	public String fetchDraftAmount()
	{
		 List<WebElement> rows=paymentsTable.findElements(By.cssSelector("tr[id^='Payments_PaymentDetailList_ct']"));
		 System.out.println(rows.size());
		 WebElement draftRow=rows.get(rows.size()-1);
		 List<WebElement> columns=draftRow.findElements(By.tagName("td"));
		 String draftAmount=columns.get(6).getText().replaceAll("[^0-9.]", "");
		 return draftAmount;
		 
	}
	
	public char clickDraftOrContinue()
	{
		char result='a';
	if(draftButton.isDisplayed())
	{
		draftButton.click();
		result= 'd';
	}
	else if(continueButton.isDisplayed())
	{
		continueButton.click();
		result= 'c';
	}
	return result;
	
}
}
	
	
	
	
